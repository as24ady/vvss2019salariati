package rair2182MV;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.*;

public class GetEmployeesTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private int supposedSizeOfRepo;

    @Before
    public void setUp() {
        try {
            employeeRepository = new EmployeeMock();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        controller = new EmployeeController(employeeRepository);

        try {
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Pacuraru;Marcel;1981202890876;ASISTENT;2000"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Dumitrescu;Razvan;1961202789087;LECTURER;2500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Ionescu;Vlad;1991202890876;LECTURER;2000"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Pacea;Larisa;2961202789087;ASISTENT;3500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Georgescu;Flavius;2961202890876;TEACHER;4500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Puscas;Laura;0961224890876;TEACHER;2500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Radaut;George;1961121369830;ASISTENT;2500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Popovici;Petcu;1961203202020;ASISTENT;2500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Manciu;Martinel;1961102020202;ASISTENT;1500"));
            employeeRepository.addEmployee(Employee.getEmployeeFromString("Laslo;Ford;1234567894321;LECTURER;2500"));
            supposedSizeOfRepo = 10;

        } catch (Exception e) {
            System.out.println("eror initializing data");
        }
    }

    @Test
    public void getEmployeesWBT(){
        List<Employee> employeesList = null;
        try {
            employeesList = controller.getEmployeesList();
            assertEquals(employeesList.size(), supposedSizeOfRepo);
        } catch (RepositoryException e) {
            fail();
        }
    }

    @Test
    public void getEmployeesBBT(){
        List<Employee> employeesList = null;
        try {
            employeesList = controller.getEmployeesList(3);
            fail();
        } catch (RepositoryException e) {
            assertEquals(e.getMessage(), "Unable to order the objects by the type you requeasted.");
        }
    }

}
