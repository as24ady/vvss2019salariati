package rair2182MV;

import static org.junit.Assert.*;

import salariati.exception.EmployeeException;
import salariati.exception.RepositoryException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

public class AddEmployeeTestBBT {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	private Employee employee;
	
	@Before
	public void setUp() {
		try {
			employeeRepository = new EmployeeMock();
			employee = new Employee("Roman", "Adrian", "1961121010203", DidacticFunction.ASISTENT, "3600");
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();

		try {
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Pacuraru;Marcel;1981202890876;ASISTENT;2000"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Dumitrescu;Razvan;1961202789087;LECTURER;2500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Ionescu;Vlad;1991202890876;LECTURER;2000"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Pacea;Larisa;2961202789087;ASISTENT;3500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Georgescu;Flavius;2961202890876;TEACHER;4500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Puscas;Laura;0961224890876;TEACHER;2500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Radaut;George;1961121369830;ASISTENT;2500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Popovici;Petcu;1961203202020;ASISTENT;2500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Manciu;Martinel;1961102020202;ASISTENT;1500"));
			employeeRepository.addEmployee(Employee.getEmployeeFromString("Laslo;Ford;1234567894321;LECTURER;2500"));

		} catch (Exception e) {
			System.out.println("eror initializing data");
		}
	}

	//ECP valid
	@Test
	public void testValidEmployee() {
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(false);
		}
	}
	//ECP non-valid
	@Test
	public void testInvalidCNP_letter() {
		employee.setCnp("196a124010203");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}
	}
	//ECP non-valid
	@Test
	public void testInvalidSalary_zero() {
		employee.setSalary("0");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}

	}
	//ECP non-valid
	@Test
	public void testInvalidSalary_letter() {
		employee.setSalary("asd4");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}

	}




	//BVA valid
	@Test
	public void testValidSalary_1() {
		employee.setSalary("1");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(false);
		}

	}
	//BVA valid
	@Test
	public void testValidSalary_2() {
		employee.setSalary("2");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(false);
		}

	}
	//BVA non-valid
	@Test
	public void testInvalidSalary_minus_1() {
		employee.setSalary("-1");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}

	}
	//BVA non-valid
	@Test
	public void testInvalidSalary_CNP_empty() {
		employee.setCnp("");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}

	}

	//BVA non-valid
	@Test
	public void testInvalidSalary_CNP_12digits() {
		employee.setCnp("196112401020");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}
	}

	//BVA non-valid
	@Test
	public void testInvalidSalary_CNP_14digits() {
		employee.setCnp("19611240102030");
		try{
			controller.addEmployee(employee);
		}catch(RepositoryException e){
			assertTrue(true);
		}
	}




}
