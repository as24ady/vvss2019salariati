package salariati.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeController {
	private EmployeeValidator employeeValidator;
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
		employeeValidator = new EmployeeValidator();
	}
	
	public void addEmployee(Employee employee) throws RepositoryException {
		if(employeeValidator.isValid(employee))
			employeeRepository.addEmployee(employee);
		else
			throw new RepositoryException("Campuri invalide");
	}

	/*
	returns a list of employees:
	if type == 1 => desc order by salary, asc order by age
	if type == 2 => desc order by salary, desc order by age
	if type missing -> type = 1

	else returns RepositoryException("Unable to order the objects by the type you requeasted.")
	 */
	public List<Employee> getEmployeesList() throws RepositoryException {
		return getEmployeesList(1);
	}

	/*
	returns a list of employees:
	if type == 1 => desc order by salary, asc order by age
	if type == 2 => desc order by salary, desc order by age
	if type missing -> type = 1

	else returns RepositoryException("Unable to order the objects by the type you requeasted.")
	 */
	public List<Employee> getEmployeesList(int type) throws RepositoryException {
		switch (type){
			case 1: return getEmployees_DescSalary_AscAge();
			case 2: return getEmployees_DescSalary_DescAge();
			default: throw new RepositoryException("Unable to order the objects by the type you requeasted.");
		}
	}

	private List<Employee> getEmployees_DescSalary_AscAge(){
		List<Employee> employees = employeeRepository.getEmployeeList();

		Collections.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				if(o2.getSalary().compareTo(o1.getSalary()) == 0){
					int posStartCnp = 1;
					int posEndCnp = 7;
					return o2.getCnp().substring(posStartCnp, posEndCnp).compareTo(o1.getCnp().substring(posStartCnp, posEndCnp));
				}
				return o2.getSalary().compareTo(o1.getSalary());
			}
		});


		return employeeRepository.getEmployeeList();
	}

	private List<Employee> getEmployees_DescSalary_DescAge(){
		List<Employee> employees = employeeRepository.getEmployeeList();

		Collections.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				if(o2.getSalary().compareTo(o1.getSalary()) == 0){
					int posStartCnp = 1;
					int posEndCnp = 7;
					return o1.getCnp().substring(posStartCnp, posEndCnp).compareTo(o2.getCnp().substring(posStartCnp, posEndCnp));
				}
				return o2.getSalary().compareTo(o1.getSalary());
			}
		});


		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws RepositoryException {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}

	public void deleteEmployee(Employee employee) throws RepositoryException {
		employeeRepository.deleteEmployee(employee);
	}

	public Employee getEmployeeByCnp(String cnp) throws RepositoryException {
		return employeeRepository.getEmployeeByCnp(cnp);
	}
}
