package salariati.view;

import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu {
    static EmployeeRepositoryInterface employeesRepository;
    static EmployeeController employeeController;

    public Menu() throws RepositoryException {
        employeesRepository = new EmployeeImpl();
        employeeController = new EmployeeController(employeesRepository);
    }

    public void run(){
        Integer option;
        EmployeeValidator validator = new EmployeeValidator();

        while(true) {
            System.out.println("Meniu: (tastati cifra corespunzatoare actiunii pe care doriti sa o faceti)");
            System.out.println("1. Vezi salariatii ordonati descrescator dupa salariu si crescator dupa varsta");
            System.out.println("2. Adauga salariat");
            System.out.println("3. Modifica functia didactica a unui salariat");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                option = Integer.parseInt(br.readLine());
            } catch (Exception e) {
                System.out.println("Ati introdus un caracter gresit. Reincercati.");
                continue;
            }

            try {
                switch (option) {
                    case 1:
                        for (Employee _employee : employeeController.getEmployeesList())
                            System.out.println(_employee.toString());
                        System.out.println("-----------------------------------------");
                        break;

                    case 2:{

                            System.out.println("Nume: ");
                            String name = br.readLine();
                            System.out.println("Prenume: ");
                            String firstname = br.readLine();
                            System.out.println("Cnp: ");
                            String cnp = br.readLine();
                            System.out.println("Functie didactica: (ASISTENT, LECTURER, TEACHER, CONFERENTIAR)");
                            String function = br.readLine();
                            System.out.println("Salariu: ");
                            String salary = br.readLine();

                            Employee employee = new Employee(name, firstname, cnp, DidacticFunction.valueOf(function), salary);
                            if (validator.isValid(employee)) {
                                employeeController.addEmployee(employee);
                            } else {
                                System.out.println("Eroare la procesarea datelor introduse.");
                            }


                        break;}
                    case 3: {
                        System.out.println("Cnp: ");
                        String cnp = br.readLine();

                        Employee oldEmployee = employeeController.getEmployeeByCnp(cnp);
                        Employee employee = oldEmployee;

                        System.out.println("Noua functie didactica: (ASISTENT, LECTURER, TEACHER, CONFERENTIAR)");
                        String function = br.readLine();

                        employee.setFunction(DidacticFunction.valueOf(function));
                        if (validator.isValid(employee)) {
                            employeeController.modifyEmployee(oldEmployee, employee);
                        }
                        break;
                    }

                    default:
                        System.out.println("Ati introdus un caracter gresit. Reincercati.");
                        break;
                }
            }catch(RepositoryException | IOException e){
                System.out.println("Eroare. " + e.getMessage() + '\n' + "Reincercati.\n");
            }
        }
    }
}
