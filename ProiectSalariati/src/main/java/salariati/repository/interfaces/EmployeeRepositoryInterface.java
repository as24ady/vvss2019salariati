package salariati.repository.interfaces;

import java.util.List;

import salariati.exception.RepositoryException;
import salariati.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee) throws RepositoryException;
	void deleteEmployee(Employee employee) throws RepositoryException;
	void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws RepositoryException;
	List<Employee> getEmployeeList();
	Employee getEmployeeByCnp(String criteria) throws RepositoryException;

}
