package salariati.repository.mock;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import salariati.enumeration.DidacticFunction;

import salariati.exception.EmployeeException;
import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {


	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;

	public EmployeeMock() throws RepositoryException {
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();

		readFromFile();
	}

	public void readFromFile() throws RepositoryException {


	}

	@Override
	public boolean addEmployee(Employee employee) throws RepositoryException {
		try {
			if ( employeeValidator.isValid(employee)) {

				try{
					getEmployeeByCnp(employee.getCnp());
					throw new RepositoryException("Exista deja un angajat cu acest CNP");
				}catch (RepositoryException re){
					if(re.getMessage().equals("Nu exista un agnajat cu acest CNP")){
						employeeList.add(employee);

						return true;
					}else{
						throw new RepositoryException(re.getMessage());
					}
				}


			}
		} catch ( RepositoryException e) {
			throw new RepositoryException(e.getMessage());
		}
		return false;

	}

	@Override
	public void deleteEmployee(Employee employee) throws RepositoryException {
		throw new RepositoryException("Nu exista metoda DELETE.");
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws RepositoryException {

		if(!employeeValidator.isValid(newEmployee)){//1
			throw new RepositoryException("Datele noului angajat nu respecta criterile de stocare.");//2
		}

		if(!oldEmployee.getCnp().equals(newEmployee.getCnp())){//3
			throw new RepositoryException("CNP-ul nu se poate modifica!");//4
		}

		for (int i=0; i < employeeList.size(); i++){//5
			if(employeeList.get(i).getCnp().equals(oldEmployee.getCnp())){//6
				employeeList.set(i, newEmployee);//7
				writeAllListToFile();
				return;
			}
		}

		throw new RepositoryException("Nu exista angajatul pe care doriti sa il modificati.");//8

	}//9

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public Employee getEmployeeByCnp(String cnp) throws RepositoryException {
		for (Employee e: employeeList){
			if(e.getCnp().equals(cnp)){
				return e;
			}
		}
		throw new RepositoryException("Nu exista un agnajat cu acest CNP");
	}

	private void writeAllListToFile(){

	}


}
