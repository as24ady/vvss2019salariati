package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;

import salariati.exception.RepositoryException;
import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	private String filename = "in.txt";

	public EmployeeImpl() throws RepositoryException {
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();

		readInitData();
		//readFromFile();
	}

	public void readInitData() throws RepositoryException {
		try {
			addEmployee(Employee.getEmployeeFromString("Pacuraru;Marcel;1981202890876;ASISTENT;2000"));
			addEmployee(Employee.getEmployeeFromString("Dumitrescu;Razvan;1961202789087;LECTURER;2500"));
			addEmployee(Employee.getEmployeeFromString("Ionescu;Vlad;1991202890876;LECTURER;2000"));
			addEmployee(Employee.getEmployeeFromString("Pacea;Larisa;2961202789087;ASISTENT;3500"));
			addEmployee(Employee.getEmployeeFromString("Georgescu;Flavius;2961202890876;TEACHER;4500"));
			addEmployee(Employee.getEmployeeFromString("Puscas;Laura;0961224890876;TEACHER;2500"));
			addEmployee(Employee.getEmployeeFromString("Radaut;George;1961121369830;ASISTENT;2500"));
			addEmployee(Employee.getEmployeeFromString("Popovici;Petcu;1961203202020;ASISTENT;2500"));
			addEmployee(Employee.getEmployeeFromString("Manciu;Martinel;1961102020202;ASISTENT;1500"));
			addEmployee(Employee.getEmployeeFromString("Laslo;Ford;1234567894321;LECTURER;2500"));
		} catch (EmployeeException e) {
			e.printStackTrace();
		}
	}

//	public void readFromFile() throws RepositoryException {
//
//		try {
//			Scanner input = new Scanner(new File(filename));
//
//			while(input.hasNextLine()) {
//				String line = input.nextLine();
//				if(line.length()<1){
//					break;
//				}
//
//				Employee employee = Employee.getEmployeeFromString(line);
//				employeeList.add(employee);
//			}
//			input.close();
//
//		} catch (FileNotFoundException | EmployeeException e) {
//			throw new RepositoryException(e.getMessage()+ " " +e.getStackTrace());
//		}
//	}

	@Override
	public boolean addEmployee(Employee employee) throws RepositoryException {
		try {
			if ( employeeValidator.isValid(employee)) {

				try{
					getEmployeeByCnp(employee.getCnp());
					throw new RepositoryException("Exista deja un angajat cu acest CNP");
				}catch (RepositoryException re){
					if(re.getMessage().equals("Nu exista un agnajat cu acest CNP")){
						employeeList.add(employee);
//						Writer output;
//						output = new BufferedWriter(new FileWriter(filename, true));
//						output.append(employee.toString()+"\n");
//						output.close();
						return true;
					}else{
						throw new RepositoryException(re.getMessage());
					}
				}


			}
		} catch (RepositoryException e) {
			throw new RepositoryException(e.getMessage());
		}
		return false;

	}

	@Override
	public void deleteEmployee(Employee employee) throws RepositoryException {
		throw new RepositoryException("Nu exista metoda DELETE.");
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws RepositoryException {
		if(employeeList.contains(oldEmployee)){
			employeeList.remove(oldEmployee);
			employeeList.add(newEmployee);
			//writeAllListToFile();
		}else{
			throw new RepositoryException("Nu exista angajatul pe care doriti sa il modificati.");
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public Employee getEmployeeByCnp(String cnp) throws RepositoryException {
		for (Employee e: employeeList){
			if(e.getCnp().equals(cnp)){
				return e;
			}
		}
		throw new RepositoryException("Nu exista un agnajat cu acest CNP");
	}


//	private void writeAllListToFile(){
//		File file = new File("in.txt");
//
//		BufferedWriter writer = null;
//		try {
//			writer = new BufferedWriter(new FileWriter(filename));
//
//			for (Employee employee:employeeList)
//				writer.write(employee.toString()+"\n");
//
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}
}
