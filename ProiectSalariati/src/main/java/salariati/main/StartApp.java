package salariati.main;

import salariati.exception.RepositoryException;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.view.Menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functicia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {

	public static void main(String[] args) {
		Menu menu = null;
		try {
			menu = new Menu();
			menu.run();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

}
